package main

import (
	// font tools
	"github.com/golang/freetype"
	"github.com/golang/freetype/truetype"
	"golang.org/x/image/font"
	// image tools
	"image"
	"image/draw"
	"image/png"
	// file reading/writing
	"os"
	// logging
	"log"
	"fmt"
	// for reading in the font (freetype.ParseFont requires it to be []byte)
	"io/ioutil"
	// for reading from stdin
	"bufio"
)

const (
	// where the font is
	FONTPATH = "BlenderPro.ttf"
	// where the template is
	TEMPLATEPATH = "template.png"
	// where the output should go
	OUTPUTPATH = "output.png"
	// the img coords to start drawing the text
	STARTX = 241
	STARTY = 282
	// font details incl. line height
	FONTSIZE = 32
	DPI = 72
	LINEHEIGHT = 36
)


// Load the font and return it.
func loadFont(path string) (*truetype.Font, error) {
	var ttf *truetype.Font
	// read font file
	fontfile, err := ioutil.ReadFile(path)
	if err != nil {
		return ttf, fmt.Errorf("Failed to load font: %w", err)
	}
	// parse font into a *truetype.Font
	ttf, err = freetype.ParseFont(fontfile)
	if err != nil {
		return ttf, fmt.Errorf("Failed to read font: %w", err)
	}
	return ttf, nil
}

// Return the template as a draw.Image.
func getTemplate() (draw.Image, error) {
	var dimg draw.Image
	file, err := os.Open(TEMPLATEPATH)
	if err != nil {
		return dimg, fmt.Errorf("Failed to open template file: %w", err)
	}
	defer file.Close()
	img, err := png.Decode(file)
	if err != nil {
		return dimg, fmt.Errorf("Failed to read template file: %w", err)
	}
	// hack?: we have to cast this to a draw.Image to get write capabilities
	// this is intentional on their part but it is also kind of dumb imo
	// (https://stackoverflow.com/a/30243259)
	dimg, ok := img.(draw.Image)
	if !ok {
		return dimg, fmt.Errorf("Image is not drawable: %w", err)
	}
	return dimg, err
}

// Save an image.Image as a PNG.
func savePNG(path string, img image.Image) error {
	file, err := os.Create(path)
	if err != nil {
		return fmt.Errorf("Failed to create output file: %w", err)
	}
	err = png.Encode(file, img)
	if err != nil {
		return fmt.Errorf("Failed to write image to output file: %w", err)
	}
	return nil
}

func main() {
	// get the template
	img, err := getTemplate()
	if err != nil {
		log.Fatal(err)
	}
	// get our font
	ttf, err := loadFont(FONTPATH)
	if err != nil {
		log.Fatal(err)
	}
	// instantiate text drawer
	drawer := &font.Drawer{
		Dst: img,
		Src: image.Black,
		Dot: freetype.Pt(STARTX, STARTY),
		Face: truetype.NewFace(ttf, &truetype.Options{
			Size:    FONTSIZE,
			DPI:     DPI,
			Hinting: font.HintingFull,
		}),
	}
	// draw each line in stdin with the given line height and line number
	linenumber := 0
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		drawer.DrawString(scanner.Text())
		linenumber += 1
		// reposition draw point
		drawer.Dot = freetype.Pt(STARTX, STARTY + (LINEHEIGHT * linenumber))
	}
	// check for errors - best not to save a busted file
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	// save and exit
	err = savePNG(OUTPUTPATH, img)
	if err != nil {
		log.Fatal(err)
	}
}
