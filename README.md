# cyberpunkgen

A simple command line tool for generating Cyberpunk 2077 delay announcements.

I figured CDPR might need it. They're probably too busy crunching their workers
to keep making too many more of these by hand.

## Usage

Build it (you need Go installed):

```bash
git clone https://codeberg.org/bclindner/cyberpunkgen
go build
```

Next you need the TTF version of the announcement font, Blender Pro. find it and
save it as "BlenderPro.ttf" in the repo directory. You could also save any other
TTF but it would look wrong.

To run, pipe a text file into it:

```bash
./cyberpunkgen < examples/announcement.txt
```

Easy as that. I'll probably break this out into a library later.

You can use the fold command to make sure your text fits the image.
About 130 bytes seems to work fine.

```bash
fold -w 130 -s -b singleline.txt | ./cyberpunkgen
```
